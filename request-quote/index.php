<?php $pagetitle = "Request a Quote" ?>

<?php include '../inc/top.php'; ?>

<?php include '../inc/header.php'; ?>

<div class="container">
	<div class="row" id="search-bar">
		<?php include '../inc/search.php'; ?>
	</div>
	<div class="row">
		<div class="col-xs-2" id="page-nav">
			<?php include '../inc/nav.php'; ?>
		</div>
		<div class="col-xs-10" id="main-content">
			<h1>Request A Quote</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, dicta, ea, hic, consequatur distinctio excepturi laudantium eligendi blanditiis molestiae autem laborum vitae modi id animi ab? Obcaecati, illum ratione in.</p>
			<h4>Below is a list of the product(s) you have requested quotes for.</h4>
			<ul class="product-listing-quote">
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<button type="button" class="close" aria-hidden="true">&times;</button>
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum, deserunt, sunt, nostrum, at numquam fuga ea fugit veritatis vitae tempore minima ullam repudiandae magni adipisci aut quas cumque placeat aspernatur.</p>
						</div>
						<span class="product-quantity">Quantity Available: #</span>
					</div>
				</li>
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<button type="button" class="close" aria-hidden="true">&times;</button>
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
						</div>
						<span class="product-quantity">Quantity Available: #</span>
					</div>
				</li>
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<button type="button" class="close" aria-hidden="true">&times;</button>
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
						</div>
						<span class="product-quantity">Quantity Available: #</span>
					</div>
				</li>
			</ul>
			<h4>Fill out your contact information and any questions/comments and we'll get back to you shortly.</h4>
			<form role="form">
			  <div class="form-group">
			    <label for="FirstName">First Name</label>
			    <input type="text" class="form-control" id="firstName" placeholder="Enter First Name">
			  </div>
			  <div class="form-group">
			    <label for="LastName">Last Name</label>
			    <input type="text" class="form-control" id="lastName" placeholder="Enter Last Name">
			  </div>
			  <div class="form-group">
			    <label for="email">Email Address</label>
			    <input type="email" class="form-control" id="email" placeholder="Email">
			  </div>
			  <div class="form-group">
			    <label for="phone1">Phone #1</label>
			    <input type="text" class="form-control" id="phone_1" placeholder="Phone #1">
			  </div>
			  <div class="form-group">
			    <label for="phone2">Phone #2</label>
			    <input type="text" class="form-control" id="phone_2" placeholder="Phone #2">
			  </div>
			  <div class="form-gropu">
			  	<label for="Comments">Comments/Feedback</label>
			  	<textarea class="form-control" rows="5" placeholder="Comments"></textarea>
			  </div>
			  <br>
			  <button type="submit" class="btn btn-default btn-primary btn-large">Submit Request for Bid</button>
			</form>
		</div>
	</div>
	<div class="row">
		<?php include '../inc/footer.php'; ?>
	</div>
</div>

<?php include '../inc/bottom.php'; ?>