<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>ATS Supply : <?php echo $pagetitle; ?></title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

	<link href='http://fonts.googleapis.com/css?family=Marck+Script|Titillium+Web:400,300,500,600,800' rel='stylesheet' type='text/css'>
    <link href="/assets/css/bootstrap.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="/assets/ico/favicon.png">

	<!-- Google Analytics Here -->

</head>
<body>