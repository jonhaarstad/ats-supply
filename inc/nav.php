<h2>Available Products</h2>
<ul>
	<li><a href="#" data-toggle="collapse" data-target="#manufacturer"><span class="caret"></span> By Manufacturer</a>
		<ul id="manufacturer" class="collapse">
			<li><a href="#">Brand A</a></li>
			<li><a href="#">Brand B</a></li>
			<li><a href="#" class="active">Goulds</a>
				<ul class="">
					<li><a href="#" class="active">3196</a>
						<ul>
							<li><a href="#">Impeller</a></li>
							<li><a href="#">Casing</a></li>
							<li><a href="#">Other</a></li>
							<li><a href="#">Something</a></li>
						</ul>
					</li>
					<li><a href="#">Item #</a></li>
					<li><a href="#">Item #</a></li>
				</ul>			
			</li>
		</ul>
	</li>
	<li><a href="#" data-toggle="collapse" data-target="#type"><span class="caret"></span> By Type</a>
		<ul id="type" class="collapse">
			<li><a href="#">Name</a></li>
			<li><a href="#">Name</a></li>
			<li><a href="#">Name</a></li>
			<li><a href="#">Name</a></li>
			<li><a href="#">Name</a></li>
			<li><a href="#">Name</a></li>
			<li><a href="#">Name</a></li>
		</ul>
	</li>
</ul>
<h2>How This Site Works</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, tempora, eaque, error dolore ullam similique nam sed doloribus sunt illo vero voluptate? Rem, ratione non. Quae, modi dignissimos eos voluptatum.</p>
<h2>Contact Us</h2>
	<p>Phone: 333-333-3333<br>
	   Fax: 444-444-4444<br>
	   Email: <a href="#">name@mail.com</a></p>