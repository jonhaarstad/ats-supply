<div id="page-header">
	<div class="container">
		<div class="row">
			<div class="col-sm-2 col-xs-6 logo-block">
				<h1 id="logo"><a href="/"><img src="/assets/images/logo.png" alt="ATS Surplus"></a></h1>
			</div>
			<div class="col-sm-6 col-xs-6 subnav">
				<ul>
					<li><a href="/index.php">Home</a></li>
					<li><a href="/request-quote">Request A Quote</a></li>
					<li><a href="/contact-us">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-sm-4 header-phone hidden-xs">
				<h2>Call Us: <span>1-888-999-7777</span></h2>
			</div>
		</div>
		<div class="row visible-xs">
			<h2>Call Us: <span>1-888-999-7777</span></h2>
		</div>
	</div>
</div>