<?php include '../inc/top.php'; ?>

<?php include '../inc/header.php'; ?>

<div class="container">
	<div class="row" id="search-bar">
		<?php include '../inc/search.php'; ?>
	</div>
	<div class="row">
		<div class="col-xs-2" id="page-nav">
			<?php include '../inc/nav.php'; ?>
		</div>
		<div class="col-xs-10" id="main-content">
			<h1>Contact Us</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, velit, architecto, reprehenderit, enim eligendi nisi neque debitis nemo iste voluptas veniam ut pariatur animi quae possimus assumenda alias. Obcaecati, perspiciatis.</p>
			<h3><strong>Email:</strong> <a href="#">info@mail.com</a><br>
				<strong>Phone:</strong> 333-333-3333</h3>
			<hr>
			<h4>Header Here</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, dolor, quaerat accusamus laborum molestiae est deserunt numquam temporibus dolores doloremque eum provident similique illum iure culpa cumque odio enim nulla!</p>	
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, dolor, quaerat accusamus laborum molestiae est deserunt numquam temporibus dolores doloremque eum provident similique illum iure culpa cumque odio enim nulla!</p>
		</div>
	</div>
	<div class="row">
		<?php include '../inc/footer.php'; ?>
	</div>
</div>

<?php include '../inc/bottom.php'; ?>