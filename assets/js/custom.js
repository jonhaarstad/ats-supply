// Expander code for text truncation
$.expander.defaults.slicePoint = 150;

$(document).ready(function() {

  // customize options
  $('div.expandable p').expander({
    slicePoint:       120,  // default is 100
    expandPrefix:     ' ', // default is '... '
    expandText:       '...more', // default is 'read more'
    collapseTimer:    5000, // re-collapses after 5 seconds; default is 0, so no re-collapsing
    userCollapseText: '[^]'  // default is 'read less'
  });

});