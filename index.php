<?php include 'inc/top.php'; ?>

<?php include 'inc/header.php'; ?>

<div class="container">
	<div class="row" id="search-bar">
		<?php include 'inc/search.php'; ?>
	</div>
	<div class="row">
		<div class="col-sm-2" id="page-nav">
			<?php include 'inc/nav.php'; ?>
		</div>
		<div class="col-sm-10" id="main-content">
			
			<!-- THIS IS WHERE THE PRODUCT LIST WILL GO ...follow the span classes within a ul/li setup.-->
			<ul class="product-listing">
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<span class="product-title">[TITLE-HERE]</span>
						<span class="product-manufacturer">Manufacturer: <a href="#">[MANUFACTURER-NAME]</a></span>
						<span class="product-model-num">Model: <a href="#">[ITEM-MODEL-HERE]</a></span>
						<div class="product-description expandable">
							<p><!--[PRODUCT-DESCRIPTION-HERE]-->Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, fugit, dolorum, libero, in tempora ipsa esse omnis maxime ab accusamus iusto veritatis ad natus provident optio quod nemo numquam perferendis?</p>
						</div>
						<span class="product-quantity">Quantity: [QUANTITY-HERE]</span>
						<span class="product-request-quote"><a class="btn-request" href="#"><span></span>Quote</a></span>
					</div>
				</li>
				
				<!-- BELOW IS JUST FOR EXAMPLES...YOU CAN DELETE THIS ONCE YOU'VE PLUGGED IN THE DYNAMIC ELEMENTS -->
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description expandable">
							<p><!--[PRODUCT-DESCRIPTION-HERE]-->Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, fugit, dolorum, libero, in tempora ipsa esse omnis maxime ab accusamus iusto veritatis ad natus provident optio quod nemo numquam perferendis?</p>
						</div>
						<span class="product-quantity">Quantity: #</span>
						<span class="product-request-quote"><a href="#" class="btn-request"><span></span>Quote</a></span>
					</div>
				</li>
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description expandable">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
						</div>
						<span class="product-quantity">Quantity: #</span>
						<span class="product-request-quote"><a href="#" class="btn-request"><span></span>Quote</a></span>
					</div>
				</li>
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer expandable">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description expandable">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
						</div>
						<span class="product-quantity">Quantity: #</span>
						<span class="product-request-quote"><a href="#" class="btn-request"><span></span>Quote</a></span>
					</div>
				</li>
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer expandable">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description expandable">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
						</div>
						<span class="product-quantity">Quantity: #</span>
						<span class="product-request-quote"><a href="#" class="btn-request"><span></span>Quote</a></span>
					</div>
				</li>
				<li>
					<div class="product-pic">
						<img src="http://lorempixel.com/output/abstract-q-c-200-200-10.jpg" alt="Product-Title-Here" class="product-image">
					</div>
					<div class="product-data">
						<span class="product-title">Title Here</span>
						<span class="product-manufacturer expandable">Manufacturer: <a href="#">Name-Here</a></span>
						<span class="product-model-num">Model: <a href="#">Number-Here</a></span>
						<div class="product-description expandable">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, recusandae magni sapiente doloremque temporibus consequatur aliquam ipsum. Deserunt, numquam, eum, necessitatibus sed vero magni optio ab tempora laboriosam fuga amet.</p>
						</div>
						<span class="product-quantity">Quantity: #</span>
						<span class="product-request-quote"><a href="#" class="btn-request"><span></span>Quote</a></span>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<?php include 'inc/footer.php'; ?>
	</div>
</div>

<?php include 'inc/bottom.php'; ?>